// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA6bSOGAgbWCcaNd9FB27M3GPBGfA7hTyo",
    authDomain: "testangular-5dfa5.firebaseapp.com",
    databaseURL: "https://testangular-5dfa5.firebaseio.com",
    projectId: "testangular-5dfa5",
    storageBucket: "testangular-5dfa5.appspot.com",
    messagingSenderId: "712968662365",
    appId: "1:712968662365:web:9e02bec029840bd6fc8cb4",
    measurementId: "G-TR379TJX9Y"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
