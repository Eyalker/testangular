import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) { }

  email:string;
  password:string; 
  hide:true;
  onSubmit(){
    this.authService.Login(this.email,this.password);
    // this.router.navigate(['/']);
  }

  ngOnInit() {
  }
}
